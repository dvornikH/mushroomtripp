﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mushrooom
{
    internal class Character
    {
        private double hp;
        private double maxhp = 100;
        private double action;
        private double maxaction = 100;
        private double agility;
        private int damage;
        private int razbros;
        Random rand = new Random();
        public double Hp { get { return hp; } set { if (hp % 1 != 0) { hp = Math.Round(value); } else { hp = value; } } }
        public double Action { get { return action; } set { if (action % 1 != 0) { hp = Math.Round(value); } else { action = value; } } }
        public double Agility { get { return agility; } set { if (agility % 1 != 0) { agility = Math.Round(value); } else { agility = value; } } }
        public int Damage { get { return damage; } set { damage = value; } }
        public int Razbros { get { return razbros; } set { razbros = value; } }
        public Character(double hepe, double aktion, double uklonenie, int damag, int razbros)
        {
            Hp = hepe;
            Action = aktion;
            Agility = uklonenie;
            Damage = damag;
            Razbros = razbros;
        }
        public bool AgilityCheck()
        {
            if (rand.Next(101) <= Agility) { return true; } else return false;
        }
        public void ChangeHealth(double value)
        {
            Hp += value;
            if (Hp > maxhp)
            {
                Hp = maxhp;
            }
        }
        public void ChangeAction(double value)
        {
            Action += value;
            if (Action > maxaction)
            {
                Action = maxaction;
            }
        }
        public void ChangeAgility(double value)
        {
            Agility += value;
            if (Agility > 50)
            {
                Agility = 50;
            }
        }
        public void ChangeDamage(int value)
        {
            Damage += value;
        }
        public void ChangeRazbros(int value)
        {
            Razbros += value;
        }
        public double FinalDamage()
        {
            return Convert.ToDouble(rand.Next((Damage - razbros), (Damage + razbros + 1)));
        }
        public string CharStats()
        {
            return "Здоровье: " + Hp + ". Стамина: " + Action + ". Шанс уклонения: " + Agility + " %.";
        }
        public string CharStatsTrue()
        {
            return "Здоровье: " + Hp + ". Стамина: " + Action + ". Шанс уклонения: " + Agility + " %. Урон: " + Damage + ". Разброс: " + Razbros + " в обе стороны.";
        }
    }
}

