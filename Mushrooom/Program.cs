namespace Mushrooom
{

    internal class Program
    {
        static Character geroi = new Character(100, 100, 17, 17, 5);
        static int MushroomCollection = 0;
        static void Main(string[] args)
        {
            Introduction();
            while (MushroomCollection < 10)
            {
                Console.WriteLine("Тебе бы мухоморы поискать. Так что ищи, хотя выбор безусловно есть! У тебя есть целых 3 варианта действий:");
                Console.WriteLine("1. Идти весело искать мухоморы.");
                Console.WriteLine("2. Основательно отдохнуть.");
                Console.WriteLine("3. Скептически оценить свое состояние.");
                int option = GetIntInRange(3);
                if (option == 1)
                {
                    MushroomSearch();
                }
                else if (option == 2)
                {
                    TakeRest();
                }
                else if (option == 3)
                {
                    Console.WriteLine(geroi.CharStatsTrue());
                }
            }
            Console.WriteLine("На подходе к вашему лагерю с мухоморами ты обнаруживаешь... Мухоморокрада! Елки палки, он сейчас умыкнет все мухоморы!");
            Console.WriteLine("Не теряя времени зря, ты бросаешься на него со своей тридцатикилограммовой палкой.");
            Muhomorokrad_Boss();
            Console.WriteLine("Переступая труп наглого вора мухоморов, ты двинулся к своему котлу.");
            Console.WriteLine("Ну вот ты и собрал свои драгоценные мухоморы. Осталось сварить зелье. Спустя 4 часа непрерывного бодяженья адового блюда вонь в лесу стояла такая, что кора с берёз отслаивалась.");
            Console.WriteLine("Не скрывая триумф на своем лице и предвкушая, ты быстро, но смакуя все отвратности этого пойла выпил всё залпом. ... И тут тебя унесло.");
            Console.WriteLine("Далёкие звёзды кружат хороводы перед твоими глазами, а тело чувствует себя так, словно находится где-то на границе Вселенной.");
            Console.ReadLine();
            Console.WriteLine("Оклемавшись спустя тысячу лет, ты обнаруживаешь себя в мрачной камере, рядом лежит какой-то бомж, а по ту сторону камеры, ухмыляясь, стоит представитель стражи правопорядка.");
            //Console.WriteLine("Как оказалось, запах исходит от тебя.");
            Console.WriteLine("- Хехе, ну ты и соня. Тебя даже вчерашний шторм не разбудил! - с еле сдерживаемым смехом произнес, товарищ-майор. Посмотрел на тебя немного и вышел за дверь, закрыв ее за собой, тем самым погрузив помещение во тьму. Все остальные органы чувств обострились и ты учуял шлейф из аромата сушеных мухоморов, который оставил после себя майор..");
        }
        static void Introduction() //вот тут вставляете предысторию и ридлайны чтобы выдавало по 1 приложению за раз и продолжало историю по нажатию любой кнопки
        {
            Console.WriteLine("ВСТУПЛЕНИЕ");
            Console.WriteLine("В начале лета 2023 года в самой большой стране на всем земном шаре случилась катастрофа. Все началось с того, что в России развернулась  гражданская война, и не успело пройти и 2 дней с ее начала, как ситуацией в России воспользовались иностранные интервенты. Наша страна была одной ногой в могиле, нас загнали в угол, а как известно, крыса, загнанная в угол бьет больнее всего! Самолет с той самой красной кнопкой был поднят в воздух, и кнопку нажали.");
            Console.ReadLine();
            Console.WriteLine("Мгновение! Все. Больше привычного нам мира нет, весь цивилизованный мир перестал существовать в тот момент, когда была нажата кнопка, ведь следом за Россией ядерное оружие использовали все, и те, кто его имел и те, кто его формально не имел. Самые удачливые из рода человеческого успели спрятаться в бункерах, конечно это были в основном богатые люди, но спустя то время, которое понадобилось, чтобы припасы в бункере закончились, все богатые стали бедными, нечем было больше владеть. И вот один, самый бедный из бедных, был выдворен из бункера на разведку аккурат к тому моменту времени, когда припасы начали подходить к концу.");
            Console.ReadLine();
            Console.WriteLine("И вышел он, и посмотрел наверх, и увидел он странных незнакомых птиц. А птицы эти куда-то летели, хотя вокруг была лишь серая пустыня, и пошел тогда храбрый разведчик за птицами. Набрел он на какой-то остров, весь покрытый зеленью. И все было такое яркое, так контрастировало с серой действительностью, что была видна даже граница между этим волшебным лесом и остальным миром. Не думая ни о чем, завороженный путник пересекает границу и идет вглубь леса, он забыл, что может заблудиться, и то, что лес посреди ядерной пустыни - это ненормально. Шел он шел, но ничего не нашел - кругом одни грибы и деревья, хотя даже те на проверку оказались грибами, и земля из грибов, и трав из грибов - все из мельчайших грибов или грибов побольше. Ситуация все меньше походила на чудо и все больше напоминала кошмар. Побродивши несколько часов странник вымотался и проголодался, голод был адский. Делать нечего - он нашел грибочек по-аппетитнее, очень уж напомнил  ему его любимое печенье, 'чеховские грибочки'.");
            Console.ReadLine();
            Console.WriteLine("Вспышка в глазах. Тьма. Пробуждение. Рядом откусанный гриб... Кричит. От боли. Откусанный гриб кричит от боли.,, и радости? Гриб умирал, это было видно, но перед смертью он решил поговорить: 'Все эти грибы - бывшие люди, умершие во время ядерной войны, они насильно заточены в грибах некой сущностью, как и я доселе, но ты спас меня, правда сам оказался здесь заперт в этом несуществующем царстве грибов, чтобы разрушить этот мир, освободить луши людей и выбраться отсюда, тебе нужно сварить зелье из мухоморов и выпить его. Так что я умер, а ты теперь ищи мухоморы, парень'");
            Console.ReadLine();
            Svinomat_Encounter();
        }
        static int GetIntInRange(int optionsNumber) //вот это пока не трогать, оно не дает выбрать то, что выбирать не нужно
        {
            string input = Console.ReadLine();
            int number = -1;
            bool isConverted = int.TryParse(input, out number);
            bool isInRange = number >= 1 && number <= optionsNumber;
            while (!isConverted || !isInRange)
            {
                Console.WriteLine("Неверная опция.");
                input = Console.ReadLine();
                isConverted = int.TryParse(input, out number);
                isInRange = number >= 1 && number <= optionsNumber;
            }
            return number;
        }

        public static void GameOver() //после смерти из приложения мы выходим
        {
            if (geroi.Hp <= 0)
            {
                Console.WriteLine("Вы погибли");
                Console.ReadLine();
                Environment.Exit(0);
            }
        }
        public static void Svinomat_Encounter()
        {
            Console.WriteLine("На вас выбегает Свиномать!");
            Character vrag = new Character(50, 30, 6, 15, 3);
            while (vrag.Hp > 0)
            {
                Console.WriteLine("Герой. " + geroi.CharStats());
                Console.WriteLine("Свиномать. " + vrag.CharStats());
                Console.WriteLine("Действия:");
                Console.WriteLine("1. Атака.");
                Console.WriteLine("2. Убежать.");
                Console.WriteLine("3. Ждать.");
                int option = GetIntInRange(3);
                if (option == 1)
                {
                    if (geroi.Action > 0)
                    {
                        HeroHit();
                        if (!vrag.AgilityCheck())
                        {
                            vrag.ChangeHealth(-geroi.FinalDamage());
                            geroi.ChangeAction(-20);
                        }
                        else
                        {
                            Console.WriteLine("Промах! Свиномать не получает урона!");
                        }
                        Console.ReadLine();
                    }
                    else { HeroTired(); }
                }
                if (option == 2)
                {
                    HeroRun();
                    Console.ReadLine();
                    break;
                }
                if (option == 3)
                {
                    HeroWait();
                    geroi.ChangeAction(50);
                    Console.ReadLine();
                }
                Console.WriteLine("Свиномать атакует!");
                if (!geroi.AgilityCheck())
                {
                    geroi.ChangeHealth(-vrag.FinalDamage());
                }
                else
                {
                    HeroDodge();
                }
                Console.ReadLine();
                GameOver();
            }
        }
        public static void Medved_Doed_Encounter()
        {
            Console.WriteLine("Из чащи выходит Медвед-Доед!");
            Character vrag = new Character(100, 20, 4, 25, 1);
            while (vrag.Hp > 0)
            {
                Console.WriteLine("Герой. " + geroi.CharStats());
                Console.WriteLine("Медвед-Доед. " + vrag.CharStats());
                Console.WriteLine("Действия:");
                Console.WriteLine("1. Атака.");
                Console.WriteLine("2. Убежать.");
                Console.WriteLine("3. Ждать.");
                int option = GetIntInRange(3);
                if (option == 1)
                {
                    if (geroi.Action > 0)
                    {
                        HeroHit();
                        if (!vrag.AgilityCheck())
                        {
                            vrag.ChangeHealth(-geroi.FinalDamage());
                            geroi.ChangeAction(-20);
                        }
                        else
                        {
                            Console.WriteLine("Промах! Медвед-Доед не получает урона!");
                        }
                        Console.ReadLine();
                    }
                    else { HeroTired(); }
                }
                if (option == 2)
                {
                    HeroRun();
                    Console.ReadLine();
                    break;
                }
                if (option == 3)
                {
                    HeroWait();
                    geroi.ChangeAction(50);
                    Console.ReadLine();
                }
                if (vrag.Action == 20)
                {
                    Console.WriteLine("Медвед-Доед атакует!");
                    if (!geroi.AgilityCheck())
                    {
                        geroi.ChangeHealth(-vrag.FinalDamage());
                    }
                    else
                    {
                        HeroDodge();
                    }
                    Console.ReadLine();
                    vrag.ChangeAction(-20);
                }
                else
                {
                    vrag.ChangeAction(10);
                    Console.WriteLine("Медвед-Доед разминает лапы...");
                }
                GameOver();
            }
            Console.WriteLine("Победа!");
        }
        public static void Krah_Bahich_Encounter()
        {
            Console.WriteLine("Сверху пикирует Крах-Бахыч!");
            Character vrag = new Character(20, 30, 86, 13, 12);
            while (vrag.Hp > 0)
            {
                Console.WriteLine("Герой. " + geroi.CharStats());
                Console.WriteLine("Крах-Бахыч. " + vrag.CharStats());
                Console.WriteLine("Действия:");
                Console.WriteLine("1. Атака.");
                Console.WriteLine("2. Убежать.");
                Console.WriteLine("3. Ждать.");
                int option = GetIntInRange(3);
                if (option == 1)
                {
                    if (geroi.Action > 0)
                    {
                        HeroHit();
                        if (!vrag.AgilityCheck())
                        {
                            vrag.ChangeHealth(-geroi.FinalDamage());
                            geroi.ChangeAction(-20);
                        }
                        else
                        {
                            Console.WriteLine("Промах! Крах-Бахыч не получает урона!");
                        }
                        Console.ReadLine();
                    }
                    else { HeroTired(); }
                }
                if (option == 2)
                {
                    HeroRun();
                    Console.ReadLine();
                    break;
                }
                if (option == 3)
                {
                    HeroWait();
                    geroi.ChangeAction(50);
                    Console.ReadLine();
                }
                if (vrag.Action == 40)
                {
                    Console.WriteLine("Крах-Бахыч достаёт снайперскую винтовку!");
                    if (!geroi.AgilityCheck())
                    {
                        geroi.ChangeHealth(-50);
                        Console.WriteLine("Бабах! Хэдшот.");
                    }
                    else
                    {
                        HeroDodge();
                    }
                    vrag.ChangeAction(-40);
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Крах-Бахыч атакует!");
                    if (!geroi.AgilityCheck())
                    {
                        geroi.ChangeHealth(-vrag.FinalDamage());
                        vrag.ChangeAction(10);
                    }
                    else
                    {
                        HeroDodge();
                    }
                    Console.ReadLine();
                }
                GameOver();
            }
            Console.WriteLine("Победа!");
        }
        public static void Muhomorokrad_Boss()
        {
            Console.WriteLine("Финальное противостояние!");
            Character vrag = new Character(500, 0, 33, 25, 2);
            while (vrag.Hp > 0)
            {
                Console.WriteLine("Герой. " + geroi.CharStats());
                Console.WriteLine("Мухоморокрад. " + vrag.CharStats());
                Console.WriteLine("Действия:");
                Console.WriteLine("1. Атака.");
                Console.WriteLine("3. Ждать.");
                int option = GetIntInRange(2);
                if (option == 1)
                {
                    if (geroi.Action > 0)
                    {
                        HeroHit();
                        if (!vrag.AgilityCheck())
                        {
                            vrag.ChangeHealth(-geroi.FinalDamage());
                            geroi.ChangeAction(-20);
                        }
                        else
                        {
                            Console.WriteLine("Промах! Мухоморокрад не получает урона!");
                        }
                        Console.ReadLine();
                    }
                    else { HeroTired(); }
                }
                if (option == 2)
                {
                    HeroWait();
                    geroi.ChangeAction(50);
                    Console.ReadLine();
                }
                if (vrag.Action == 20)
                {
                    Console.WriteLine("Мухоморокрад кидает в тебя телефонную будку!");
                    if (!geroi.AgilityCheck())
                    {
                        geroi.ChangeHealth(-50);
                        Console.WriteLine("- Иди поплачься маме, малыш!");
                    }
                    else
                    {
                        HeroDodge();
                    }
                    Console.ReadLine();
                }
                else if (vrag.Action == 50)
                {
                    Console.WriteLine("Мухоморокрад отправляет снаряды себе в тыл! Уклонение увеличено!");
                    vrag.ChangeAgility(20);
                }
                else if (vrag.Action == 100)
                {
                    Console.WriteLine("- Ну-ну, сопляк, понюхай шляпки красивых мухоморчиков!");
                    Console.WriteLine("Мухоморокрад обрушивает 28 ударов на твою голову!");
                    for (int i = 0; i < 28; i++)
                        if (!geroi.AgilityCheck())
                        {
                            geroi.ChangeHealth(-vrag.FinalDamage());
                        }
                        else
                        {
                            HeroDodge();
                        }
                    Console.ReadLine();
                    vrag.ChangeAction(-100);
                }
                else
                {
                    Console.WriteLine("Мухоморокрад атакует!");
                    if (!geroi.AgilityCheck())
                    {
                        geroi.ChangeHealth(-vrag.FinalDamage());
                        vrag.ChangeAction(10);
                    }
                    else
                    {
                        HeroDodge();
                    }
                    Console.ReadLine();
                }
                GameOver();
            }
            Console.WriteLine("Мухоморокрад повержен.");
        }
        public static void MushroomSearch()
        {
            Random random = new Random();
            int rand = random.Next(1, 101);
            if (rand >= 1 && rand <= 33)
            {
                Console.WriteLine("Вы забрели на опасную территорию!");
                rand = random.Next(1, 4);
                if (rand == 1)
                {
                    Svinomat_Encounter();
                }
                if (rand == 2)
                {
                    Medved_Doed_Encounter();
                }
                if (rand == 3)
                {
                    Krah_Bahich_Encounter();
                }
                Console.WriteLine("Битва окончена.");
                rand = random.Next(1, 6);
                if (rand == 1 || rand == 4)
                {
                    Console.WriteLine("Вы нашли мухомор!");
                    MushroomCollection += 1;
                }
                else
                {
                    Console.WriteLine("Видимо, мухомора здесь нет.");
                }
            }
            else if (rand >= 34 && rand <= 88)
            {
                rand = random.Next(1, 4);
                if (rand == 1)
                {
                    Console.WriteLine("Вы не нашли мухомор, но ваши хождения не пропадают даром! Вы стали ловчее!");
                    geroi.ChangeAgility(3);
                }
                if (rand == 2)
                {
                    Console.WriteLine("Вы не нашли мухомор, но ваши хождения не пропадают даром! Вы нашли палку поувесистее!");
                    geroi.ChangeDamage(3);
                }
                if (rand == 3)
                {
                    Console.WriteLine("Вы не нашли мухомор, но ваши хождения не пропадают даром! Ваши навыки махания палкой прокачались!");
                    geroi.ChangeRazbros(2);
                }
            }
            else if (rand >= 89)
            {
                Console.WriteLine("Джекпот! Вы нашли 2 мухомора!");
                MushroomCollection += 2;
            }
        }
        public static void TakeRest()
        {
            geroi.ChangeHealth(100);
            geroi.ChangeAction(100);

            Random rnd = new Random();
            if (rnd.Next(1,3) == 1)
            {
            Console.WriteLine("Вы чудесно отдохнули, как будто заново родились, теперь делайте то, что вам предначертано!");
            }
            else { Console.WriteLine("Вы хорошо отдохнули и готовы к новым свершениям."); }
            
        }
        public static void HeroWait()
        {
            Random rnd = new Random();
            if (rnd.Next(1,3) == 1)
            {
            Console.WriteLine("Герой затаился и ждет...");
            }
            else { Console.WriteLine("Герой выжидает..."); }
            
        }
        public static void HeroTired()
        {
            Random rnd = new Random();
            if (rnd.Next(1,3) == 1)
            {
            Console.WriteLine("Усталость одолела героя...");
            }
            else { Console.WriteLine("Ленин уставал и герой устал..."); }
            
        }
        public static void HeroRun()
        {
            Random rnd = new Random();
            if (rnd.Next(1,3) == 1)
            {
            Console.WriteLine("Герой быстро улепетывает в неизвестном направлении...");
            }
            else { Console.WriteLine("Герой оперативно покидает поле боя..."); }
            
        }
        public static void HeroHit()
        {
            Random rnd = new Random();
            if (rnd.Next(1,3) == 1)
            {
            Console.WriteLine("Герой размашисто бьет сбоку...");
            }
            else { Console.WriteLine("Герой делает бьет сверху вниз..."); }
        }
        public static void HeroDodge()
        {
            Random rnd = new Random();
            if (rnd.Next(1,3) == 1)
            {
                Console.WriteLine("Промах! Герой не получает урона!");
            }
            else { Console.WriteLine("Промах! Герой чувствует себя отлично, здоровья не потерял."); }
        }


    }
}
